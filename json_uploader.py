import yaml
import os
import sys
import glob
import re
import threading
import time
import logging
import boto3
import contextlib
from botocore.exceptions import ClientError
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

json_archive_basename_pattern = re.compile('(\ \[\d+\])')
last_uploaded_json_archive = ""

def json_archive_basename(json_archive):
    full_path = re.sub(json_archive_basename_pattern,"",json_archive)
    return os.path.basename(full_path)

def get_latest_json_archive(folder_path):
    attempts = 0
    json_archive_folder = os.path.join(folder_path, 'JSON ARCHIVES')
    while attempts < 3:
        try:
            archive_files = glob.glob(json_archive_folder + '/*')
            latest_json_archive = max(archive_files, key=os.path.getctime)
            return latest_json_archive
        except IOError:
            attempts += 1
            time.sleep(1)
            print("Could not not stat latest JSON archive, trying again")


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            print("\033[1;34;40m Upload %s  %s / %s  (%.2f%%)" % (
                os.path.basename(self._filename), sizeof_fmt(self._seen_so_far), sizeof_fmt(self._size),
                percentage), end="\r")
            sys.stdout.flush()

def upload_file(file_name, bucket, object_name=None):
    s3_client = boto3.client('s3',
                             aws_access_key_id=aws_access_id,
                             aws_secret_access_key=aws_secret_key)
    try:
        response = s3_client.upload_file(file_name, bucket, object_name, Callback=ProgressPercentage(file_name))
        print("")
    except ClientError as e:
        logging.error(e)
        return False
    return True


def on_created(event):
    global last_uploaded_json_archive
    file_name = os.path.basename(event.src_path)
    dir_name = os.path.dirname(event.src_path)
    time.sleep(0.5)
    latest_json_archive = get_latest_json_archive(dir_name)
    latest_json_archive_name = os.path.basename(latest_json_archive)
    if latest_json_archive != last_uploaded_json_archive:
        print(f"\033[1;32;40m Angles created {file_name}")
        print(f"\033[1;32;40m Looking for latest json archive...")
        print(f"\033[1;32;40m Found it:")
        print(f"\033[1;32;40m {latest_json_archive_name}")
        upload_file(latest_json_archive, aws_s3_bucket, file_name)
        last_uploaded_json_archive = latest_json_archive
        print(f"\033[1;34;40m {json_archive_basename(latest_json_archive)} upload complete")

if __name__ == "__main__":

    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)

    aws_s3_bucket = cfg['s3_bucket']
    aws_access_id = cfg['aws_access_key_id']
    aws_secret_key = cfg['aws_secret_access_key']

    if len(sys.argv) > 1:
        json_target_upload_folder = sys.argv[1]
        print("The target folder is:")
        print(json_target_upload_folder)
    elif 'json_target_upload_folder' not in cfg:
        print("No json_target_upload_folder key found in config.yml.")
        print("Check the config.yml.example")
        exit()
    elif not cfg['json_target_upload_folder']:
        print("Found json_target_upload_folder key, but it is blank.")
        print("Add the folder path to the config.yml")
        exit()
    else:
        json_target_upload_folder = cfg['json_target_upload_folder']
        print("S3 Upload service running. The target folder is:")
        print(json_target_upload_folder)

    patterns = ["*.json"]
    ignore_patterns = ["\.?"]
    ignore_directories = True
    case_sensitive = False
    event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
    event_handler.on_created = on_created
    json_observer = Observer()
    json_observer.event_queue.maxsize=1
    json_observer.schedule(event_handler, json_target_upload_folder, recursive=False)
    json_observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        json_observer.stop()
        print("")
        print("S3 uploading services now stopped!")
    json_observer.join()